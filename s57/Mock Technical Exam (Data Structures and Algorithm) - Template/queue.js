let collection = [];

// Write the queue functions below.


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};

function isEmpty() {
  return collection.length === 0;
}

function enqueue(item) {
  collection.push(item);
  return collection
}

function print() {
  return(collection);
}

function size() {
  return collection.length;
}

function dequeue(item) {
  return collection.shift();
}

function front() {
  return collection[0];
}


