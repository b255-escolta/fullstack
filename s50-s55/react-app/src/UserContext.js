import React from 'react';

// creates a context object
// a context object as the name states is a data type of an object that can be used to store info that can shared to other components within this app
// the context object is a different approach to passing info between components and allows easier access by avoiding the use of prop-drilling
const UserContext = React.createContext();

// the "Provider" component allows other components to consume/use the context object and supply the necessary info needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;