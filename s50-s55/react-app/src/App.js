import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom'; 
import React from 'react';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import CourseView from './components/CourseView';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/Error';
import { useState, useEffect } from 'react';

import './App.css';
import { UserProvider } from './UserContext';

function App() {
// state hook for the user state thats defined here for a global scope
// initialized as an object with properties from the local storage
// this will be used to store the user info and will be used for validating if a user is logged in on the app or not

  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    ide: null,
    isAdmin: null
  });

  // function for clearing local storage on logout
  const unsetUser = () => {
    localStorage.clear()
  }


// user to check if the user info is properoly stored upon login
  useEffect(() => {
    console.log(user);
    console.log(localStorage)
  }, [user])


  return (
    <UserProvider value ={{user, setUser, unsetUser}}>
        <Router>
          <Container fluid>
            <AppNavBar />
            <Routes>
                <Route path = "/" element={<Home/>} />
                <Route path = "/courses" element={<Courses/>} />
                <Route path = "/courseView/:courseId" element={<CourseView/>} />
                <Route path = "/register" element={<Register/>} />
                <Route path = "/login" element={<Login/>} />
                <Route path = "/logout" element={<Logout/>} />
                <Route path = "/*" element={<PageNotFound/>} />
            </Routes>
          </Container>
        </Router>
    </UserProvider>
  );
}

export default App;
